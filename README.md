# Universe 
This is the official repository of Soullinux stable packages. Universe repository contains all the packages of the core, extra, community and multilib packages. You can use this repo on other Arch based distros aswell. To do so, add these lines to your /etc/pacman.conf file
```bash
[universe-stable]
SigLevel = Optional TrustAll
Server = https://gitlab.com/soullinux/universe-stable/-/raw/stable/$arch
``` 
